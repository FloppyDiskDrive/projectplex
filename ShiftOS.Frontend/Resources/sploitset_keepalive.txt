/* Plex setup data file
 * 
 * This file holds information used by a Plex "Installer" for setting up a system component.
 *
 */

 {
	Name: "Sploitset Force Heartbeat exploit",
	Description: "This is an exploit for the sploitset system exploitation utility which causes the remote system to force the connection to stay alive by sending heartbeat requests even after the firewall has timed out the connection. Useful against those pesky connection timers...",
	SourceType: "ShiftoriumUpgrade",
	Source: "sploitset_keepalive"
 }